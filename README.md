这里列出一部分我学习的公开课课程，主要涉及数据科学、计算机科学


## Coursera

[个人主页](https://www.coursera.org/user/265eaac283c350b7f99d9baefafb4855)

### Specializations

#### Machine Learning

[证书](https://www.coursera.org/account/accomplishments/specialization/6ZT2V6S4DZUX)


课程列表：

* Foundations: A Case Study Approach
* Regression
* Classification
* Clustering & Retrieval

#### Data Science

[证书](https://www.coursera.org/account/accomplishments/specialization/YJVCLCWCZQQT)

课程列表：

* The Data Scientist’s Toolbox
* R Programming
* Getting and Cleaning Data
* Exploratory Data Analysis
* Reproducible Research
* Statistical Inference
* Regression Models
* Practical Machine Learning
* Developing Data Products
* Data Science Capstone

### Fundamentals of Computing

[证书](https://www.coursera.org/account/accomplishments/specialization/AY5HXFFQPN6W)

课程列表：

* An Introduction to Interactive Programming in Python (Part 1)
* An Introduction to Interactive Programming in Python (Part 2)
* Principles of Computing (Part 1)
* Principles of Computing (Part 2)
* Algorithmic Thinking (Part 1)
* Algorithmic Thinking (Part 2)
* The Fundamentals of Computing Capstone Exam

#### Others

* Algorithms: Design and Analysis (Part1&2, Stanford)
* Introduction to Natural Language Processing
* Text Retrieval and Search Engines
* Internet History, Technology, and Security
* Think Again: How to Reason and Argue
* The Power of Microeconomics
* ...

## edX

[个人主页](https://profile.edx.org/u/adalee2future)

课程列表：

* [Introduction to Linux](https://verify.edx.org/cert/96f8bff79dc245d6ad03490c185195e9)
* [Introduction to Big Data with Apache Spark](https://verify.edx.org/cert/47d07b6485ec436da5e247f1f25efbdf)
* [Scalable Machine Learning (Apache Spark)](https://verify.edx.org/cert/217f1bb85c7348648a34cfe1e6ac6553)
* [Text Mining & Analytics](https://verify.edx.org/cert/edea9e2ef815421c96e06106e7cf3185)
* [Introduction to Functional Programming](https://courses.edx.org/certificates/893fd754de124d9cac4486d4138fd62a)


## Udacity

#### Data Analyst Nanodegree

[证书](https://graduation.udacity.com/confirm/CDPLJCH)

项目列表：

* [P1 Test a Perceptual Phenomenon](http://adalee2future.github.io/udacity_data_analyst/P1%20Test%20a%20Perceptual%20Phenomenon.html)
* [P2 Analyzing the NYC Subway Dataset](http://adalee2future.github.io/udacity_data_analyst/P2%20Analyzing%20the%20NYC%20Subway%20Dataset.html)
* [P3 Wrangle OpenStreetMap Data](http://adalee2future.github.io/udacity_data_analyst/P3_Wrangle_OpenStreetMap_Data.pdf)
* [P4 Explore and Summarize Data](http://adalee2future.github.io/udacity_data_analyst/P4_Explore_and_Summarize_White_Wine_Data.html)
* [P5 Identifying Fraud from Enron Email](http://adalee2future.github.io/udacity_data_analyst/P5_Identifying_Fraud_from_Enron_Email.pdf)
* [P6 Make Effective Data Visualization](https://github.com/adalee2future/udacity_d3_prj)
* [P7 Design an A/B Test](http://adalee2future.github.io/udacity_data_analyst/AB_Test.pdf)

## MongoDB University

* [MongoDB for Python Developers](http://university.mongodb.com/course_completion/8fb756855a33484c92111e09a391cc64)
* [MongoDB for DBAs](https://university.mongodb.com/course_completion/25fa2c351bcc40929a8bfe9a7f059cc6)